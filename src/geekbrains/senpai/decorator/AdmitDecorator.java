package geekbrains.senpai.decorator;

import geekbrains.senpai.model.Visitor;
import geekbrains.senpai.service.AdmitService;

public class AdmitDecorator implements AdmitService {

    private static int numberOfVisitors;
    private AdmitService admitService;

    public AdmitDecorator(AdmitService admitService) {
        this.admitService = admitService;
    }

    @Override
    public void admit(Visitor visitor) {
        admitService.admit(visitor);
        numberOfVisitors++;
    }

    @Override
    public void release(Visitor visitor) {
        admitService.release(visitor);
    }

    public static void result() {
        System.out.println("Количество посетителей: " + numberOfVisitors);
        clear();
    }

    private static void clear() {
        numberOfVisitors = 0;
    }
}
