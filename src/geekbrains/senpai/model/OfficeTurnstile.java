package geekbrains.senpai.model;

import geekbrains.senpai.observe.AdmitObservable;
import geekbrains.senpai.observe.VisitorObserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 2. Турникеты для офисов, срабатывают на вход и выход при предьявлении сотрудником пропуска с id
public class OfficeTurnstile extends Turnstile implements AdmitObservable {
    private Map<Integer, Visitor> visitorMap = new HashMap<>();
    private List<VisitorObserver> visitorObserverList = new ArrayList<>();

    public OfficeTurnstile() {

    }

    OfficeTurnstile(int serialNum, String location) {
        super(serialNum, location);
    }

    @Override
    public void admit(Visitor visitor) {
        Visitor inOfficeVisitor = getVisitorMap().get(visitor.getId());
        if (inOfficeVisitor == null) {
            System.out.println("Пожалуйста проходите в офис!");
            getVisitorMap().put(visitor.getId(), visitor);
            notifyAdmitObservers(visitor);
        } else {
            System.out.println("Прохода в офис нет!");
        }
    }

    @Override
    public void release(Visitor visitor) {
        Visitor inOfficeVisitor = getVisitorMap().get(visitor.getId());
        if (inOfficeVisitor != null) {
            System.out.println("Пожалуйста выходите из офиса!");
            getVisitorMap().remove(visitor.getId());
            notifyReleaseObservers(visitor);
        } else {
            System.out.println("Выхода из офиса нет!");
        }
    }

    public Map<Integer, Visitor> getVisitorMap() {
        return visitorMap;
    }

    @Override
    public void registerObserver(VisitorObserver visitorObserver) {
        visitorObserverList.add(visitorObserver);
    }

    @Override
    public void notifyAdmitObservers(Visitor visitor) {
        visitorObserverList.forEach(visitorObserver -> visitorObserver.admitObserver(visitor));
    }

    @Override
    public void notifyReleaseObservers(Visitor visitor) {
        visitorObserverList.forEach(visitorObserver -> visitorObserver.releaseObserver(visitor));
    }
}
