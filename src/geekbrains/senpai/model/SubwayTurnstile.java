package geekbrains.senpai.model;

public class SubwayTurnstile {

    public void enter(Visitor visitor) {
        int diff = visitor.getCashCard() - 25;
        if (diff < 0) {
            System.out.println("Прохода нет, на вашем счету недостаточно денег");
        } else {
            System.out.println("Проходите");
            visitor.setCashCard(diff);
        }
    }
}
