package geekbrains.senpai.model;

import geekbrains.senpai.service.AdmitServiceOneWayImpl;

// 1. Турникеты для торговых центров - срабатывают на вход только при накоплении 3-х посетителей(основной режим)
public class ShoppingCentreTurnstile extends Turnstile {

    public ShoppingCentreTurnstile(){}

    public ShoppingCentreTurnstile(int serialId, String location) {
        super(serialId, location);
        admitService = new AdmitServiceOneWayImpl();
    }
}
