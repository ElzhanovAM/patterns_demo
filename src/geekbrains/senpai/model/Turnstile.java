package geekbrains.senpai.model;

import geekbrains.senpai.service.AdmitService;

public abstract class Turnstile implements AdmitService {
    private int serialNum;
    private String location;

    AdmitService admitService;

    public Turnstile() {
    }

    Turnstile(int serialNum, String location) {
        this.serialNum = serialNum;
        this.location = location;
    }

    public void admit(Visitor visitor) {
        admitService.admit(visitor);
    }

    public void release(Visitor visitor) {
        admitService.release(visitor);
    }

    public int getSerialNum() {
        return serialNum;
    }

    public String getLocation() {
        return location;
    }

    public void setAdmitService(AdmitService admitService) {
        this.admitService = admitService;
    }

    public void setSerialNum(int serialNum) {
        this.serialNum = serialNum;
    }
}
