package geekbrains.senpai.model;

public class Visitor {
    private int id;
    private String name;
    private int cashCard;

    public Visitor(int id, String name, int cashCard) {
        this.id = id;
        this.name = name;
        this.cashCard = cashCard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCashCard() {
        return cashCard;
    }

    public void setCashCard(int cashCard) {
        this.cashCard = cashCard;
    }
}
