package geekbrains.senpai.observe;

import geekbrains.senpai.model.Visitor;

public interface AdmitObservable {
    void registerObserver(VisitorObserver visitorObserver);
    void notifyAdmitObservers(Visitor visitor);
    void notifyReleaseObservers(Visitor visitor);
}
