package geekbrains.senpai.observe;

import geekbrains.senpai.model.Visitor;

import java.util.Date;

public class CustomerVisitorObserver implements VisitorObserver {

    @Override
    public void admitObserver(Visitor visitor) {
        System.out.println("Сотрудник " + visitor.getName() + " зашел в офис " + new Date());
    }

    @Override
    public void releaseObserver(Visitor visitor) {
        System.out.println("Сотрудник " + visitor.getName() + " вышел из офиса " + new Date());
    }
}
