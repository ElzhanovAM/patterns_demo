package geekbrains.senpai.observe;

import geekbrains.senpai.model.Visitor;

public interface VisitorObserver {
    void admitObserver(Visitor visitor);
    void releaseObserver(Visitor visitor);
}
