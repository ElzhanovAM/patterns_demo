package geekbrains.senpai.factory;

import geekbrains.senpai.model.Turnstile;
import geekbrains.senpai.service.AdmitService;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTurnstileFactory {
    private List<AdmitService> turnstiles = new ArrayList<>();

    public List<AdmitService> createTurnstileList(int number, Class<? extends Turnstile> turnstileClass) {
        for (int i = 0; i < number; i++) {
            try {
                turnstiles.add(createTurnstile(turnstileClass, i));
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
        return turnstiles;
    }

    abstract AdmitService createTurnstile(Class<? extends Turnstile> turnstileClass, int i) throws IllegalAccessException, InstantiationException;
}
