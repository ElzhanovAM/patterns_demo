package geekbrains.senpai.factory;

import geekbrains.senpai.model.Turnstile;
import geekbrains.senpai.service.AdmitService;

public class TurnstileFactory extends AbstractTurnstileFactory {

    @Override
    AdmitService createTurnstile(Class<? extends Turnstile> turnstileClass, int i) throws IllegalAccessException, InstantiationException {
        Turnstile turnstile = turnstileClass.newInstance();
        turnstile.setSerialNum(i);
        return turnstile;
    }
}
