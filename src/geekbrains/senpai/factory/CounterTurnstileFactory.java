package geekbrains.senpai.factory;

import geekbrains.senpai.decorator.AdmitDecorator;
import geekbrains.senpai.model.Turnstile;
import geekbrains.senpai.service.AdmitService;
import geekbrains.senpai.service.AdmitServiceOneWayImpl;

public class CounterTurnstileFactory extends AbstractTurnstileFactory {
    @Override
    AdmitService createTurnstile(Class<? extends Turnstile> turnstileClass, int i) throws IllegalAccessException, InstantiationException {
        Turnstile turnstile = turnstileClass.newInstance();
        turnstile.setSerialNum(i);
        turnstile.setAdmitService(new AdmitServiceOneWayImpl());
        AdmitDecorator admitDecorator = new AdmitDecorator(turnstile);
        return admitDecorator;
    }
}
