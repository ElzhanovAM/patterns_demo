package geekbrains.senpai.service;

import geekbrains.senpai.model.Visitor;

public class AdmitServiceSecureImpl implements AdmitService {
    @Override
    public void admit(Visitor visitor) {
        Visitor inOfficeVisitor = visitorMap.get(visitor.getId());
        if (inOfficeVisitor == null) {
            System.out.println("Пожалуйста проходите в офис!");
            visitorMap.put(visitor.getId(), visitor);
        } else {
            System.out.println("Прохода в офис нет!");
        }
    }

    @Override
    public void release(Visitor visitor) {
        Visitor inOfficeVisitor = visitorMap.get(visitor.getId());
        if (inOfficeVisitor != null) {
            System.out.println("Пожалуйста выходите из офиса!");
            visitorMap.remove(visitor.getId());
        } else {
            System.out.println("Выхода из офиса нет!");
        }
    }
}
