package geekbrains.senpai.service;

import geekbrains.senpai.model.Visitor;

public class AdmitServiceOneWayImpl implements AdmitService {
    private static final int MAX_VISITOR_ADMIT = 2;

    @Override
    public void admit(Visitor visitor) {
        visitorMap.put(visitorMap.size()+1, visitor);
        int numberOfVisitors = visitorMap.size();
        if (numberOfVisitors > MAX_VISITOR_ADMIT) {
            System.out.println("Добро пожаловать!");
            visitorMap.clear();
        }
    }

    @Override
    public void release(Visitor visitor) {

    }
}
