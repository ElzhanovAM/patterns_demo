package geekbrains.senpai.service;

import geekbrains.senpai.model.Visitor;

import java.util.HashMap;
import java.util.Map;

public interface AdmitService {
    Map<Integer, Visitor> visitorMap = new HashMap<>();
    void admit(Visitor visitor);
    void release(Visitor visitor);
}
