package geekbrains.senpai.seria;

import geekbrains.senpai.model.Visitor;
import geekbrains.senpai.service.AdmitService;

import java.util.ArrayList;
import java.util.List;

public class TurnstileSeria implements AdmitService {
    private List <AdmitService> turnstilSeria = new ArrayList<>();

    @Override
    public void admit(Visitor visitor) {
        for (AdmitService aTurnstilSeria : turnstilSeria) {
            aTurnstilSeria.admit(visitor);
        }
        turnstilSeria.clear();
    }

    @Override
    public void release(Visitor visitor) {
    }

    public void add(AdmitService admitService) {
        turnstilSeria.add(admitService);
    }

    public void addAll(List<AdmitService> admitServices) {
        turnstilSeria.addAll(admitServices);
    }
}
