package geekbrains.senpai;

import geekbrains.senpai.adapter.AdmitAdapter;
import geekbrains.senpai.decorator.AdmitDecorator;
import geekbrains.senpai.factory.AbstractTurnstileFactory;
import geekbrains.senpai.factory.CounterTurnstileFactory;
import geekbrains.senpai.factory.TurnstileFactory;
import geekbrains.senpai.model.*;
import geekbrains.senpai.observe.CustomerVisitorObserver;
import geekbrains.senpai.seria.TurnstileSeria;
import geekbrains.senpai.service.AdmitService;
import geekbrains.senpai.service.AdmitServiceSecureImpl;

import java.util.List;

public class TurnstileSimulator {

    public static void main(String[] args) {

        TurnstileSimulator turnstileSimulator = new TurnstileSimulator();
        //turnstileSimulator.shoppingCentreStimulator();
        //turnstileSimulator.subwayStimulator();
        //turnstileSimulator.numberOfVisitorsStimulator();
        //turnstileSimulator.seriesOfTurnstileSimulator();
        turnstileSimulator.admitVisitorsSignal();

    }

    // Предистория: У нас с вами компания по выпуску турникетов, бизнес идет хорошо, заказы поступают
    // На данном этапе выпуск налажен по 2-м сериям:
    // 1. Турникеты для торговых центров
    // 2. Турникеты для офисов


    // Стратегия
    // Заказчик дал задание сделать гибкую легко изменяемую программу для турникета в торговом центре,
    // днем он работает для посетителей, ночью для ночного персонала - то есть как офисный турникет!
    private void shoppingCentreStimulator() {
        Visitor petya = new Visitor(1, "Петя", 0);
        Visitor masha = new Visitor(2, "Маша", 0);
        Visitor john = new Visitor(3, "Джон", 0);

        Turnstile shoppingCentreTurnstile = new ShoppingCentreTurnstile(1, "Ikea");
        shoppingCentreTurnstile.admit(petya);
        shoppingCentreTurnstile.admit(masha);
        shoppingCentreTurnstile.admit(john);

        shoppingCentreTurnstile.setAdmitService(new AdmitServiceSecureImpl());
        shoppingCentreTurnstile.admit(petya);
        shoppingCentreTurnstile.admit(masha);
        shoppingCentreTurnstile.admit(john);
    }

    // Адаптер
    // К офисным турникетам пришло дополнение в виде турникета из метро, со встроенной программой
    // Все бы ничего только данный турникет требует денег от сотрудников для прохода на работу!!!
    private void subwayStimulator() {
        Visitor petya = new Visitor(1, "Петя", 0);
        Visitor masha = new Visitor(2, "Маша", 0);
        Visitor john = new Visitor(1, "Джон", 25);

        SubwayTurnstile subwayTurnstile = new SubwayTurnstile();
        subwayTurnstile.enter(petya);
        subwayTurnstile.enter(masha);
        subwayTurnstile.enter(john);

        AdmitService admitService = new AdmitAdapter(subwayTurnstile);
        admitService.admit(petya);
    }

    // Декоратор
    // Заказчик дал задание мониторить количество посетителей в торговом центре,
    // С формированием отчета о количестве посетителей за день для статистики посещаемости
    private void numberOfVisitorsStimulator() {
        Visitor petya = new Visitor(1, "Петя", 0);
        Visitor masha = new Visitor(2, "Маша", 0);
        Visitor john = new Visitor(3, "Джон", 0);

        AdmitService admitService = new AdmitDecorator(new ShoppingCentreTurnstile(1, "Лента"));
        admitService.admit(petya);
        admitService.admit(masha);
        admitService.admit(john);
        admitService.admit(petya);
        admitService.admit(masha);
        admitService.admit(john);
        admitService.admit(petya);
        admitService.admit(masha);
        admitService.admit(john);

        AdmitDecorator.result();
    }

    // Абстрактная фабрика, шаблонный метод, компоновщик
    // Производство турникетов расширяется.
    // Заказчик дал задание установить на турникеты систему тестового открытия для проверки всех систем
    private void seriesOfTurnstileSimulator() {

        Visitor testVisitor = new Visitor(0, "test", 0);

        TurnstileSeria turnstileSeria = new TurnstileSeria();

        SubwayTurnstile subwayTurnstile = new SubwayTurnstile();
        AdmitService admitService = new AdmitAdapter(subwayTurnstile);
        turnstileSeria.add(admitService);

        AbstractTurnstileFactory turnstileFactory = new TurnstileFactory();
        List<AdmitService> admitServices = turnstileFactory.createTurnstileList(15, OfficeTurnstile.class);
        turnstileSeria.addAll(admitServices);


        turnstileFactory = new CounterTurnstileFactory();
        List<AdmitService> admitServiceList = turnstileFactory.createTurnstileList(21, ShoppingCentreTurnstile.class);
        turnstileSeria.addAll(admitServiceList);

        turnstileSeria.admit(testVisitor);

        AdmitDecorator.result();
    }

    //Наюлюдатель
    // Заказчик попросил встроить в офисный турникет систему оповещения о сотрудниках приходящих и уходящих в офис с выводом времени
    private void admitVisitorsSignal() {
        Visitor petya = new Visitor(1, "Петя", 0);
        Visitor masha = new Visitor(2, "Маша", 0);
        Visitor john = new Visitor(1, "Джон", 0);

        CustomerVisitorObserver customerVisitorObserver = new CustomerVisitorObserver();

        OfficeTurnstile officeTurnstile = new OfficeTurnstile();

        officeTurnstile.registerObserver(customerVisitorObserver);

        officeTurnstile.admit(petya);
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        officeTurnstile.admit(masha);

        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        officeTurnstile.admit(john);

        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        officeTurnstile.release(petya);
    }
}
