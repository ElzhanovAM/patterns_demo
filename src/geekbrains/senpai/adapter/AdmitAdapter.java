package geekbrains.senpai.adapter;

import geekbrains.senpai.model.SubwayTurnstile;
import geekbrains.senpai.model.Visitor;
import geekbrains.senpai.service.AdmitService;

public class AdmitAdapter implements AdmitService {
    private SubwayTurnstile subwayTurnstile;

    public AdmitAdapter(SubwayTurnstile subwayTurnstile) {
        this.subwayTurnstile = subwayTurnstile;
    }

    @Override
    public void admit(Visitor visitor) {
        visitor.setCashCard(25);
        subwayTurnstile.enter(visitor);
    }

    @Override
    public void release(Visitor visitor) {

    }
}
